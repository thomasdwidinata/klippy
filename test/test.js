const request = require('supertest');
const assert = require('assert');
// const assert = require('chai').assert;
const fs = require('fs');
const app = require('../app');

describe('mainpage', function() {
  it("welcomes silly people", function (done) {
    request(app).get('/')
      .expect(200, done)
      // .expect('/klippy.jpg', done)
  });
})

describe('filemanager', function() {
  var lastInsert = ""
  const put = function(done) {
    request(app)
      .put('/file')
      .attach('file', './test/sampleimage/1/klippy.jpg')
      .expect(200)
      .expect('Content-Type', /json/)
      .end( function(err, response) {
        assert.strictEqual(response.body.status, 'ok');
        assert.strictEqual(response.body.statusCode, 200);
        assert.strictEqual(response.body.message.name, 'klippy.jpg');
        assert.strictEqual(response.body.message.size, 0.0045322265625);
        assert.strictEqual(response.body.message.type, 'jpeg');
        lastInsert = response.body.message.path;
        done();
      });
  }
  const get = function(done) {
    request(app)
      .get(`/file/${lastInsert}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .end( function(err, res) {
        const compareFile = function(err, data) {
          assert(res.body, data)
          return;
        }
        fs.readFileSync(lastInsert, compareFile)
        done()
      } );
  }
  const del = function(done) {
    request(app)
      .delete(`/file/${lastInsert}`)
      .expect(200)
      .expect('Content-Type', /json/)
      .end( function(err, res) {
        assert.strictEqual(res.body.status, 'ok');
        assert.strictEqual(res.body.statusCode, 200);
        assert.strictEqual(res.body.message, '');
        lastInsert = ""
        done()
      });
  }
  const patch = function(done) {
    request(app)
      .patch(`/file/${lastInsert}`)
      .attach('file', './test/sampleimage/3/patrick.jpg')
      .expect(200)
      .expect('Content-Type', /json/)
      .end( function(err, res) {
        assert.strictEqual(res.body.status, 'ok');
        assert.strictEqual(res.body.statusCode, 200);
        assert.strictEqual(res.body.message.name, 'patrick.jpg');
        assert.strictEqual(res.body.message.size, 0.00683984375);
        assert.strictEqual(res.body.message.type, 'jpeg');
        lastInsert = res.body.message.path;
        done();
      });
  }
  it("PUT: put file to the server", put)
  this.timeout(10000)
  it("GET: get file to the server", get)
  this.timeout(2000) // Set back to default
  it("DELETE: delete file from the server", del)
  it('PUT: put file to the server again', put)
  it('PATCH: update file to the server', patch)
  it("GET: get file to the server", get)
  it("DELETE: delete file from the server again for cleanup", del)
})
