global.crypto = require('crypto');

module.exports = function (length = 20) {
  return crypto.randomBytes(length).toString('hex');
}
