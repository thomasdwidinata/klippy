const jsonWrap = function(res, code, status, message) {
  res.json({
    statusCode: code,
    status: status,
    message: message
  })
}
module.exports.jsonWrap = jsonWrap;

const ok = function(res, message) {
  jsonWrap(res, 200, 'ok', message);
}
module.exports.ok = ok;

const err = function(res, message) {
  jsonWrap(res, -255, 'failure', message);
}
module.exports.err = err;

const customErr = function (res, code, message) {
  jsonWrap(res, code, 'failure', message);
}
module.exports.customErr = customErr;
