var express = require('express');
var router = express.Router();
const PutFile = require('../controller/putfile.js');
const PatchFile = require('../controller/patchfile.js');
const DeleteFile = require('../controller/deletefile.js');
const GetFile = require('../controller/getfile.js');
const upload = require('../middleware/multer');
const ipfilter = require('../middleware/ipfilter');

router.put('/', ipfilter, upload.single('file'), PutFile);
router.delete('/:public/:hash/:name', ipfilter, DeleteFile);
router.patch('/:public/:hash/:name', ipfilter, upload.single('file'), PatchFile);
router.get('/:public/:hash/:name', GetFile);

module.exports = router;
