# klippy 

### Description
A back-end system for storing and managing customer's images and documents. It automatically generates random folder name for 'extra' privacy and security.

The server is intended only to run on local network with the production server. It is NOT intended to be used as a public server as the code are currently trusting its own local network and no verification at the moment. Although the code can be easily refactored to have a verification, you should be aware that the good practise of having a file server is not to make it public.

### Technologies
The technologies, programming language, and operating system that we are using are listed below:
- Ubuntu 18.04.2 LTS
- Node.JS 10.15.3
- See `package.json` for more node modules

### Installation
to be populated later... For now, use `pm2` or manual `npm start`!

### Changes
See `CHANGES.md` for more information.

### Easter Eggs
Just find it by yourself. Read the code and the comments!

### Contributors
We proudly to presents our team members who takes their effort to develop this amazing application:
- Gamma Andhika [<gamma1210@gmail.com\>](gamma1210@gmail.com) [\(gammaandhika.com\)](https://gammaandhika.com)
- Thomas Dwi Dinata [<thomasdwidinata@gmail.com\>](mailto:thomasdwidinata@gmail.com) [\(thomasdwidinata.com\)](https://thomasdwidinata.com)
