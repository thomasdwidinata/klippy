var createError = require('http-errors');
var express = require('express');
var fs = require('fs');
var path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const config = require('./config/config');

var homeRouter = require('./routes/index');
var fileManager = require('./routes/filemanager');

console.log(`Starting up ${config.name}:${config.version}...`);

var app = express();

app.enable('trust proxy');
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' });
if(config.app === 'dev' || config.app === 'development') {
  console.log('Environment Mode: Development');
  app.use(logger('dev'));
} else if (config.app === 'prod' || config.app === 'production') {
  console.log('Environment Mode: Production');
  app.use(logger('combined', {stream: accessLogStream}));
}
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));
app.use(cookieParser());
app.use(cors());

// Define Routes here
app.use('/', homeRouter);
app.use('/file', fileManager);

// Serve magical images, comment this in case of emergency!
app.use('/static', express.static(path.join(__dirname, 'static')))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.status(404);
  res.json({statusCode: 404, status:"error", message:"Route not found"})
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = config.app === 'development' ? err : {};
  res.status(err.status || 500);
  res.json({statusCode: err.status || 500, status:"error", message: res.locals.message})
});

module.exports = app;
