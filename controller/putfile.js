const {ok, err} = require('../helper/jsonwrapper');

const putFile = function(req, res, next) {
  if(req.hasOwnProperty('file')) {
    const fileDetails = {
      path: req.file.path,
      name: req.file.filename,
      size: (req.file.size/1024000),
      type: (req.file.mimetype.split('/')[1])
    }
    ok(res, fileDetails);
  } else {
    err(res, "File is not posted to the server.")
  }
}
module.exports = putFile;
