const {ok, err, customErr} = require('../helper/jsonwrapper');
const fs = require('fs');

const getFile = async function(req, res, next) {
  var path = `${req.params.public}/${req.params.hash}/${req.params.name}`;
  fs.readFile(path, function(err, data) {
    if(err) {
      customErr(res, -2, "File not found.");
    } else {
      res.status(200);
      res.send(data);
    }
  })
}
module.exports = getFile;
