var hardCodedTemplate = "<html><head></head><body><div style=\"max-height: 100%; height: 100vh; display: flex; flex-direction: column; justify-content: center; align-items: center;\"><img src=\"../static/img/klippy.jpg\" alt=\"I'm Klippy!\" height=\"250\" width=\"250\" /><p>{{msg}}</p></div></body></html>";
const prankdomiser = [
  "Hmm, Nani?",
  "",
  "",
  "",
  "",
  "",
  "什么?",
  "",
  "",
  "",
  "<a href=\"https://youtu.be/dQw4w9WgXcQ\">https://youtu.be/dQw4w9WgXcQ</a>",
  "I'm Klippy!",
  "Onde",
  "",
  "",
  "",
  "",
  "",
  "",
]

const home = function(req, res, next) {
  var random = Math.floor(Math.random() * Math.floor(prankdomiser.length))
  res.send(hardCodedTemplate.replace('{{msg}}', prankdomiser[random]));
}
module.exports.home = home;
