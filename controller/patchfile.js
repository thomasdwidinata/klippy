const hashGen = require('../helper/hashgen');
const fs = require('fs');
const {ok, err, customErr} = require('../helper/jsonwrapper');

const getFile = function(req, res, next) {
  if(req.hasOwnProperty('file')) {
    // Get new file details
    const fileDetails = {
      path: req.file.path,
      name: req.file.filename,
      size: (req.file.size/1024000),
      type: (req.file.mimetype.split('/')[1])
    }

    var path = `${req.params.public}/${req.params.hash}`;
    var fullpath = `${path}/${req.params.name}`;

    // Delete old file on old hash
    fs.unlinkSync(fullpath, (error) => { if(error) console.log(error) });

    // Copy the new file from new hash to old hash
    fs.copyFileSync(req.file.path, `${path}/${fileDetails.name}`, (err) => {
      if (err) throw err;
    });

    // Delete the new file on new hash folder
    var del = fileDetails.path.split('/');
    fs.unlinkSync(fileDetails.path, (error) => { if(error) console.log(error) });
    fs.rmdirSync(`${del[0]}/${del[1]}`, (error) => { if(error) console.log(error) });

    // Modify the response, useful for renaming file
    fileDetails.path = `${path}/${fileDetails.name}`;
    ok(res, fileDetails);
  } else {
    err(res, "File is not posted to the server.")
  }
}
module.exports = getFile;
