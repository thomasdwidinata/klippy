const {ok, err, customErr} = require('../helper/jsonwrapper');
const fs = require('fs');

const deleteFile = function(req, res, next) {
  var path = `${req.params.public}/${req.params.hash}`;
  var fullpath = `${path}/${req.params.name}`;
  fs.unlink(fullpath, (error) => deleteInside(error, res, path));
}
module.exports = deleteFile;

// Callback function
const deleteFolder = (error, res) => {
  if(error) {
    err(res, error);
  } else {
    ok(res, "");
  }
}
const deleteInside = (error, res, path) => {
  if(error) {
    err(res, error);
  } else {
    fs.rmdir(path, (error) => deleteFolder(error, res));
  }
}
