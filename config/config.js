require('dotenv').config(); // Instatiate environment variables

let CONFIG = {} // Make this global to use all over the application

CONFIG.name         = "klippy";
CONFIG.version      = "1.0 Beta";
CONFIG.company_name = '';
CONFIG.company_official_name = '';

CONFIG.app          = process.env.APP   || 'dev';
CONFIG.port         = process.env.PORT  || '8989';


// In case the file server got smarter...
// CONFIG.db_dialect   = process.env.DB_DIALECT    || 'mysql';
// CONFIG.db_host      = process.env.DB_HOST       || 'localhost';
// CONFIG.db_port      = process.env.DB_PORT       || '3306';
// CONFIG.db_name      = process.env.DB_NAME       || 'test';
// CONFIG.db_user      = process.env.DB_USER       || 'root';
// CONFIG.db_password  = process.env.DB_PASSWORD   || '';

// In case the file server allows storage monitoring and email if almost out
// of storage...
// CONFIG.email_address  = process.env.EMAIL_ADDRESS || '';
// CONFIG.email_password  = process.env.EMAIL_PASS || '';

module.exports = CONFIG;
