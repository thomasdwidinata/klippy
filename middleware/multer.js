const multer = require('multer');
const fs = require("fs");
const hashGen = require('../helper/hashgen.js');

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log('Downloading file from client...');

    let imagePath = 'public/' + hashGen();

    if (!fs.existsSync(imagePath))
      fs.mkdirSync(imagePath);

    cb(null, imagePath);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

var limits = {
  files: 1, // allow only 1 file per request
  fileSize: 20 * 1024 * 1024 // 20 MB (max file size)
};

var upload = multer({
  storage: storage,
  limits: limits
});
module.exports = upload;
