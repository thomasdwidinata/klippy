const fs = require('fs');

let ipUri = 'private/ip.json';

let ipfilter = async function (req, res, next) {
  let ip = req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'].split(',')[0] : req.connection.remoteAddress;

  let whitelist = await readJson(ipUri);

  let valid = whitelist.includes(ip);

  if (!valid) {
    res.status(404);
    res.json({statusCode: 404, status:"error", message:"Route not found"})
  }
  next()
}
module.exports = ipfilter;

var readJson = async (path) => {
  let rawdata = fs.readFileSync(path);
  return JSON.parse(rawdata);
}
